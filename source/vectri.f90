PROGRAM VECTRI
! 
! VECTRI: VECtor borne disease infection model of ICTP TRIeste.
!
! Author: A.M.Tompkins 2011-2014
!         ICTP, Strada Costiera 11, 34152 Trieste, ITALY
!         tompkins@ictp.it
!
! --------------------------------------------------------------------------
! NOTE: This software is issued under a free-to-use license for research purposes
!       please do not distribute without prior permission from the author
!       Please email a copy of manuscripts using the software to the author and 
!       cite Tompkins and Ermert 2013 for the model description (v1.2.6)
! --------------------------------------------------------------------------
!
! Loosely based on HM2005 and Depinay 62 and Bomblies 2008 
!
! Eventual wishlist of features:
!   1) DONE: modified survival rate formula (Ermert 2010)
!   2) advection/diffusion(flight) of vector from cell to cell 
!   3) modified surface hydrology for egg laying
!      eventually this will try to represent improved conditions in drought near rivers
!      and improved conditions in rainy season elsewhere.
!   4) diffusion of infected human population to account for local migration
!   5) gravity model/relaxation of proportion p to account for long-distance migration
!   6) standard deviation of temperature to account for subgrid-scale variability and microclimates
!   7) DONE:  biting rates account for population density and are Poisson distributed.
!   8) Immunity
!
! modules explicitly the 
! 1) gonotrophic cycle: 
!   the time taken to prepare a brood (determines biting cycle)
!
! 2) sporogonic cycle (SC) (the process of fertilization
!   of the macrogametocyte, formation of the oocyst,
!   ookinete, penetration of the midgut and then the subsequent
!   development of the sporozoites which dwell in the
!   salivary glands)
!
!   variable convention
!   -------------------
!   integer parameter n
!   integer local     i
!   real parameter    r
!   real local        z
!   
!   references 
!
!   1) Hoshen and Morse (2004)
!   2) Ermert (2010)
!   3) Bomblies et al. (2008)
!   4) garki model (1974)
!   5) depinay et al. (2004)
!   6) Tompkins and Ermert (2013) <- model description at v1.2.6
!
! --- Glossary ---
!
!  Entomological data
!
!   HBR - host bite rate - number of bites per host per day
!   EIR - Entomological inoculation rate (n of infectious bites per person)
!   CSPR - Circumsporozoite protein rate (fraction eir/hbr)
!
!  Epidemiological data
!    
!
! --- order of code ---
!
! 1. write out diagnostics
! 2. read in met data for timestep
! 3. biting treatment
!    3.1 - calculation transmission probabilities
!    3.2 - Vector to human transmission
!      3.2.1 Evolution of disease in host
!      3.2.2 clearing rate 
! 4.  Sporogonic cycle
! 5.  Gonotrophic cycle
! 6.  Vector temperature dependent survival
! 7.  Vector Oviposition
! 8.  Puddle model
! 9. Larvae maturation - limitation of resourses
! 10. Larvae maturation - larvae progression
! 12. Larvae hatching
! 15. NCDF OUTPUT
!
! --- release history --- 
! v1.0 May 2011 - Basic model consisting of LMM and Bomblies and Depinay.
!               - with addition parametrization for 
!                 a) ponds, b) pop density c) zoophily d) bednets
! v1.1 Nov 2011 - all params written to netcdf global attributes
!               - all input params controllable in namelist input 
!               - simple options added for fake climate change
!               - simple migration trickle added
! v1.2 Jan 2012 - model structure rearranged to clean up and use directory structure
!               - preprocessing upgraded orographic downscaling of temperature 
! v1.21         - mode 3 upgrades to use AR4.
! 
! v1.22-1.25    - various minor corrections to output files, naming changes, and script streamlines
!               - ability to run with ECMWF seasonal forecasts.
!
! v1.26***      - stable beta release - 
!               - adjusted hydrology 
!               - Version that is documented in Tompkins and Ermert (2013)***
!
! v1.3 July 2012 - capability of exact restarts 
!                - rain and temperature are read in one slice at a time (for AR5) and not necessarily in the output 
!                - adapable output file
!                - Long term support version
!
! v1.3.1 July 2013 - Revised Larvae growth rates according to Craig et al. 1999 with 
!                    new larvae mortality functions based on data from Bayoh and Lindsay 2004.
!                 - indoor temperature included, with temperature experience a linear mix of indoor and outdoor
!
! v1.3.2 Nov 2013 - Minor bug corrections
!
! v1.3.3 April 2014 - Tidy up netcdf reading - all scale factors and latitude reversing now in pre-processing
!                     All netcdf variables now controllable from namelist
!
! v1.3.4 Oct 2014  - Bug correction on pond fraction initialization - 
!
! v1.3.5 May 2015  - New transmission assumption that assumes EI categories receive more bites than susceptibles.
!

  USE netcdf
  USE mo_constants
  USE mo_control
  USE mo_vectri
  USE mo_ncdf_tools

  IMPLICIT NONE

  ! local integers 
  INTEGER :: i,ix,iy,iyear,imonth,iday,idoy,idate,istep,iloop,idx &
&           ,iinfv,ivect,ihost &
&           ,icheck=0

  ! local real scalars
  REAL ::    zgonof, zsporof, zhostf &
&           ,zsurvp &  ! adult vector survival probability
&           ,zrain  &  ! dummy rain variables, daily
&           ,zlarvmaturef, zdel, zmasslarv, zcapacity, zbiolimit &
&           ,ztemp, ztempindoor, ztempwater & !dummy temperature, indoor temperature and water temperature
&           ,zlimit &
&           ,zvectinfectbite, zhosttrans &
&           ,zprobhost2vect, zprobvect2hostS, zprobvect2hostEI & !transmission probabilities
&           ,znnhbr, znndailyeir, znndailyeirS, znndailyeirEI, zvect2hostratio &
&           ,zpud1, zpud2, zpud3, zfyear, zflushr & ! useful puddle variables
&           ,zsurvpl, zmeanpr, zrunoff, zmig1, zmig2

! fortran timer functions
  REAL :: time1=0.0,time2=0.0
  LOGICAL :: lspinup, &  ! spin up period - turn off output
&            ltimer=.false.      ! turn the cpu timer on for the first timestep
  ! local real vectors
  REAL, ALLOCATABLE :: zarray1(:),zvector_density(:,:)
  REAL, ALLOCATABLE :: zpr(:,:) ! calculate of parasite ratio including ALL exposed (from day 1, i.e. not detectable)

  ! --------------------
  ! START OF VECTRI CODE
  ! --------------------
  CALL setup ! initial conditions for arrays

  ! allocate the 2d local arrays
  ALLOCATE(zvector_density(nlon,nlat))
  ALLOCATE(zpr(nlon,nlat))

! Spin up
  WRITE(iounit,*) 'starting run',nrun
  DO iloop=1,nrun
    WRITE(*,'(1a1,A5,I10,$)') char(13), 'step ',iloop
    IF (ltimer) CALL timer('go  ',icheck,time1,time2) !set up timer

    ! for spin up
    IF (iloop<=nyearspinup*INT(nlenspinup)/dt) THEN
      istep=MOD(iloop-1,INT(nlenspinup))+1
      IF (iloop==1) WRITE(iounit,*) 'spin up period switched on for ',nyearspinup*nlenspinup,' days' 
      lspinup=.TRUE. 
    ELSE 
      istep=iloop-nyearspinup*INT(nlenspinup)/dt
      IF (iloop==nyearspinup*INT(nlenspinup)/dt+1) WRITE(iounit,*) 'INTEGRATION STARTS' 
      lspinup=.FALSE. 
    ENDIF

    idate=ndate(istep)
    iday=istep*dt

    !IF (MOD(iday,ndaydiag)==0) WRITE(iounit,*) 'date ',idate

    !---------------------------
    ! read in met data timeslice
    !---------------------------
    CALL getdata(istep)

    !-------------------------------------------------
    ! apply simple fixed climate or population changes
    !-------------------------------------------------
    CALL climate_pop_trends(istep)

    !------------------------------------------------
    ! 1. diagnostics for the present timestep
    !    Some of these are used in the analysis
    !    While some are for diagnostic purposes only 
    !------------------------------------------------

    !-----------------------
    ! 1.1 derive diagnostics
    !-----------------------

    !-------------------------------------------------------------------------------------
    ! vector migration
    ! this will be a weighted random walk, weighted to allow clustering around populations
    ! for now we simply set a minimum  
    !-------------------------------------------------------------------------------------
    rvect=MAX(rvect,0.0)
    rvect(:,0,:,:)=MAX(rvect(:,0,:,:),rvect_min)
    rlarv=MAX(rlarv,0.0)
    zvector_density(:,:)=SUM(SUM(rvect,DIM=1),DIM=1) ! total vector number = vector density    
    zpr=1.0-rhost(0,nadult_ni,:,:)/SUM(rhost(:,nadult_ni,:,:),DIM=1) ! parasite ratio

    ! store some diagnostics:
    IF (loutput_vector) rdiag2d(:,:,ncvar_vector(2))=zvector_density  ! store vector density
    IF (loutput_larvae) rdiag2d(:,:,ncvar_larvae(2))=SUM(rlarv,DIM=1) ! total larvae number
    IF (loutput_prd)    rdiag2d(:,:,ncvar_prd(2))= &                  ! parasite ratio
    &   SUM(rhost(INT(rhost_detectd):ninfh,1,:,:),DIM=1)/SUM(rhost(:,1,:,:),DIM=1)
    IF (loutput_pr)     rdiag2d(:,:,ncvar_pr(2))=zpr
    IF (loutput_cspr)   rdiag2d(:,:,ncvar_cspr(2))=SUM(rvect(:,ninfv,:,:),DIM=1)/MAX(zvector_density(:,:),reps)
    IF (loutput_vecthostratio) rdiag2d(:,:,ncvar_vecthostratio(2))=zvector_density(:,:)/MAX(rpopdensity(:,:),reps)

    ! zoophilic rates - well actually is anthropophilic rate.
    WHERE (rpopdensity(:,:)>=0.0) &
    & rzoophilic=1.0-(1.0-rzoophilic_min)*EXP(-rpopdensity(:,:)/rzoophilic_tau)
    rbitezoo(:,:)=1.0-(1.0-rbiteratio*rzoophilic(:,:))**dt ! product useful 

    !-----------------
    ! GRIDDED LOOP !!!
    !-----------------
    DO ix=1,nlon
    DO iy=1,nlat

    IF (ix==nxdg.and.iy==nydg) THEN
      zmasslarv=0.0 !dummy statement
    ENDIF
    
    !---------------------------------------------
    ! 1.2 point diagnostics for calculations below
    !---------------------------------------------
    ! total biomass of larvae per m2 of water surface
    zmasslarv=SUM(rlarv(:,ix,iy)*rmasslarv(:))
    if (loutput_lbiomass)rdiag2d(ix,iy,ncvar_lbiomass(2))=zmasslarv

    ! proportion of infected hosts that can transmit malaria
    zhosttrans=rhost(ninfh,1,ix,iy)/MAX(SUM(rhost(:,1,ix,iy)),reps)

    ! CSPR: proportion of infected vectors that can tranmit and may bite in *this* timestep
    zvectinfectbite=rvect(0,ninfv,ix,iy)/MAX(SUM(rvect(0,:,ix,iy)),reps)

    !------------------------
    ! 2. meteorological data
    !------------------------
    zrain=MIN(MAX(rrain(ix,iy),0.0),200.0)  ! 1 day rainfall, with safety limits applied.

    ! indoor temperature - after Lunde et al. Malaria Journal 2013, 12:28
    ztempindoor=10.33+0.58*rtemp(ix,iy)

    ! water temperature 
    ztempwater=rtemp(ix,iy)+rwater_tempoffset ! water temperature

    ! temperature experience by vector is mix of indoor and outdoor 
    ztemp=MAX(rbeta_indoor*ztempindoor+(1.0-rbeta_indoor)*rtemp(ix,iy),-30.0) ! temperature on a daily timestep

    !---------------------------------------
    ! ONLY RUN MODEL IF NOT A LAKE/SEA POINT
    !---------------------------------------
    IF (rpopdensity(ix,iy)>=rpopdensity_min) THEN

    !---------------------
    ! 3. Biting treatment 
    !---------------------

    ! --------------------------
    ! Transmission probabilities
    ! --------------------------
    !  
    ! - zbiteratio of vectors get a meal
    ! - rzoophilic proportion of these on humans 
    ! - zhostinfect of the people are infected
    ! - rpt* prob of transmission
    !   
    ! zeroth box movement depends on biting ratio
    ! - will possibly adjust the survival for this category 
    !   as feeding is dangerous.
    ! - this will depend on population of vector and host - 4now constant - 
    !  

    !----------------------------------------------
    ! 3.1 set up parameters needed in this timestep
    !----------------------------------------------

    ! ---------
    ! host2vect:
    ! ---------
    ! from the host to vector - 
    ! Assume each biting vector gets ONE MEAL - simple probability 
    !
    ! zprobhost2vect is the probability of a bite leading 
    !   to host2vector transmission
    !   need to add bed net use...
    ! 
    zprobhost2vect=rbitezoo(ix,iy)*rpthost2vect*zhosttrans

    ! ---------
    ! vect2host:
    ! ---------
    !
    ! mean daily bites per host for non bednet - taking bednetuse into account
    ! recall nn=no net, as the calculation only for people not under net
    !
    znnhbr=rbiteratio*rzoophilic(ix,iy)*SUM(rvect(0,:,ix,iy))/(rpopdensity(ix,iy)*rnobednetuse)
    IF(loutput_hbr) rdiag2d(ix,iy,ncvar_hbr(2))=znnhbr

    ! daily EIR 
    znndailyeir=zvectinfectbite*znnhbr
    IF(loutput_eir) rdiag2d(ix,iy,ncvar_eir(2))=znndailyeir

    !
    ! Treat biting as a random event, therefore is distributed as a Poisson PDF.
    ! however, only need to call if vector infection rate greater than zero
    ! 
    zprobvect2hostS=0.0
    zprobvect2hostEI=0.0
    IF (znndailyeir>reps) THEN
      ! we could speed this up if rbitehighrisk=1
      znndailyeirS=znndailyeir/(1.0+(rbitehighrisk-1.0)*zpr(ix,iy))
      CALL transmission(znndailyeirS,zprobvect2hostS)

      znndailyeirEI=znndailyeirS*rbitehighrisk
      CALL transmission(znndailyeirEI,zprobvect2hostEI)

      if (.false. .and. zpr(ix,iy)>0.1 .and. znndailyeir>0.0001)then
        zlimit=zpr(ix,iy)*znndailyeirEI+(1.0-zpr(ix,iy))*znndailyeirS
        PRINT *,'mean eir, wgt, eirS, eirEI,', znndailyeir, zlimit,znndailyeirS, znndailyeirEI
        PRINT *,'zpr ',zpr(ix,iy)
        CALL transmission(znndailyeirEI,zpud1)
        PRINT *,'probs ', zpud1, zprobvect2hostS, zprobvect2hostEI 
      endif
    ENDIF

    IF (ltimer) CALL timer('tran',icheck,time1,time2) ! cpu timer

    !--------------------------------
    ! 3.2 Vector to human transmission
    !--------------------------------
    !
    !-----------------------------------
    ! 3.2.1 Evolution of disease in host
    !-----------------------------------
    zhostf=dt/ninfh
    DO ihost=1,nhost
      ! from v1.3.5 this prob is separate for S class...
      CALL advection(zprobvect2hostS,zhostf,rhost(:,ihost,ix,iy),ninfh)
    ENDDO

    IF (ltimer) CALL timer('host',icheck,time1,time2) ! cpu timer

    !--------------------
    ! 3.2.2 clearing rate 
    !--------------------
    ! As clearing rate is >> timestep can use explicit numerics
    DO i=1,ninfh
      zdel=dt/rhostclear*rhost(i,1,ix,iy) !*(1.0-zprobvect2hostEI)
      rhost(i,1,ix,iy)=rhost(i,1,ix,iy)-zdel
      rhost(0,1,ix,iy)=rhost(0,1,ix,iy)+zdel
    ENDDO

    !---------------------
    ! 4. Sporogonic cycle
    !---------------------
    ! degree day concept of Detinova (1962):
    zsporof=dt*(ztemp-rtsporo)/dsporo
    zsporof=MIN(MAX(0.0,zsporof),1.0)

    IF (zsporof>reps) THEN
      ! need temprary array since using second index...
      ALLOCATE(zarray1(0:ninfv))
      ! NOTE: ivect=0 needs special treatment as is the 
      !       only box where infection can start
      !       IF wrap around is permitted this will also 
      !       require special treatment
      DO ivect=0,ngono
        zarray1=rvect(ivect,:,ix,iy)
        SELECT CASE(ivect)
        CASE(0)
          CALL advection(zprobhost2vect,zsporof,zarray1,ninfv)
        CASE DEFAULT
          CALL advection(0.0,zsporof,zarray1,ninfv)
        END SELECT
        rvect(ivect,:,ix,iy)=zarray1
      ENDDO
      DEALLOCATE(zarray1)
    ENDIF

    IF (ltimer) CALL timer('spor',icheck,time1,time2) ! cpu timer

    !---------------------
    ! 5. Gonotrophic cycle
    !---------------------
    !
    ! note that we do not add a day for nighttime feeding (Ermert 10), NO LONGER TRUE 
    ! however, with a less than daily timestep, the gonotropic cycle should 
    ! always be rounded UP to the nearest integer number of days, i.e.
    ! egg-laying occurs in the nighttime timestep, since feeding and vector 
    ! oviposition only occurs at night... 
    !
    ! all temperature relationships will be modified to implement a 
    ! gaussian temperature spread to account for variations within the 
    ! grid cell due to topography and microclimates.  this effectively 
    ! adds a diffusive term to the parasite and eggs progressions.
    !
    ! degree day concept of Detinova (1962):

    zgonof=dt*(ztemp-rtgono)/dgono
    zgonof=zgonof/(zgonof+1.0)     ! add one day according to LMM2010
    zgonof=MIN(MAX(0.0,zgonof),1.0)

    IF(zgonof>reps) THEN
      DO iinfv=0,ninfv
      !
      ! due to truncation by daily timestep - spread box 0 by round off
      !
        idx=FLOOR(zgonof*ngono)
        zlimit=1.0/REAL(idx+1)
        rvect(0:idx,iinfv,ix,iy)=zlimit*rvect(0,iinfv,ix,iy)
        CALL advection(rbiteratio,zgonof,rvect(:,iinfv,ix,iy),ngono)
      ENDDO
    ENDIF

    IF (ltimer) CALL timer('gono',icheck,time1,time2) ! cpu timer

    !-----------------------------------------
    ! 6. Vector temperature dependent survival
    !-----------------------------------------
    ! Martins II (emert 2010) - will add all 4 major schemes
    !
    ! could make this much more elegant and generalized
    ! n power matrix calculation
    !-----------------------------------------

    IF (ztemp>rtvecsurvmin.AND.ztemp<rtvecsurvmax) THEN

      SELECT CASE(nsurvival_scheme)
      CASE(1) ! martins I
        zsurvp=rmar1(0) + rmar1(1)*ztemp + rmar1(2)*ztemp**2
      CASE(2) ! martins II
        zsurvp=EXP(-1.0/(rmar2(0)+rmar2(1)*ztemp+rmar2(2)*ztemp**2))
      CASE(3) ! Bayoh scheme
        STOP 'later bayoh scheme'
      CASE DEFAULT
        STOP 'invalid survival scheme'
      END SELECT

      ! from v1.3.5 base mortality rate added
      zsurvp=zsurvp*rvecsurv

      zsurvp=MIN(MAX(0.0,zsurvp),1.0)
      zsurvp=zsurvp**dt ! adjust by timestep:
    ELSE
      zsurvp=0.0
    ENDIF

    rvect(:,:,ix,iy)=zsurvp*rvect(:,:,ix,iy)

    !-----------------------------------------
    ! 7. Vector oviposition
    !-----------------------------------------
    !   for the moment this follows emert 2010
    !   but eventually the option of a more evolved 
    !   surface hydrology treatment will be included
    !   fuzzy is like a fractional water coverage or breeding site factor     

    ! mean eggs per laying - eventually this will draw from a sample
    IF (ltimer) CALL timer('fuzz',icheck,time1,time2) ! cpu timer

    zlimit=1.0
    IF (nlayingmax>0) zlimit=MIN(1.0,nlayingmax/SUM(rvect(ngono,:,ix,iy)))

    rlarv(0,ix,iy)=0.0
    DO iinfv=0,ninfv
      rlarv(0,ix,iy)=rlarv(0,ix,iy)+neggmn*zlimit*rvect(ngono,iinfv,ix,iy)

      ! move the female to the meal searching box zero
      rvect(0,iinfv,ix,iy)=rvect(0,iinfv,ix,iy)+rvect(ngono,iinfv,ix,iy)
      rvect(ngono,iinfv,ix,iy)=0
    ENDDO

    IF (ltimer) CALL timer('eggs',icheck,time1,time2) ! cpu timer

    !-----------------------------------------
    ! 8. Puddle model
    !-----------------------------------------
    ! puddle model, increases surface area at rate related to rainfall

    ! This is not clean, but is a quick solution to a serious bug pre-1.3.3
    ! whereby waterfrac is the archived/initialized variable but rpuddle was the prognosed variable!
    rpuddle(ix,iy)=rwaterfrac(ix,iy)-rwaterperm(ix,iy)
   
    SELECT CASE(ipud_vers)
    CASE(125) !v1.25
      ! v1.25 : original exact solution: 
      ! dw/dt = K P - I - w/tau  
      !         K: pond geometry scale factor (related to S0 and h0 in geometry model)
      !         I: maxmimum infiltration rate from ponds - was only 3 Wm/2 !!!!
      !         tau: run off timescle 4 days 

      zpud1=rwaterfrac_rate*zrain*rwaterfrac_max-rwaterfrac_infil125+rwaterfrac_min*rwaterfrac_itau
      zpud2=rwaterfrac_rate*zrain+rwaterfrac_itau
      zpud1=zpud1/zpud2
      rpuddle(ix,iy)=zpud1-(zpud1-rpuddle(ix,iy))*exp(-zpud2*dt)
    CASE(126)
      zpud1=rwaterfrac_rate*dt
      rpuddle(ix,iy)=(rpuddle(ix,iy)+zpud1*zrain*rwaterfrac_max)/(1.0+zpud1*(zrain+rwaterfrac_evap126))

    CASE(130) !v1.30
      !-----------------------------------------          
      ! dw/dt = K w^(-p/2) (fQ - w(E+fI))
      !         K: pond geometry scale factor (related to S0 and h0 in geometry model)
      !         p: pond geometry power factor (0.5-2 temporary ponds, 3-5 lakes)
      !         f: proportion of maximum pond area factor 1-w/w_max
      !         Q: run off - calculated from SCS formula Q=(P-0.2S)^2/(P+0.8S)
      !         E: evaporation from ponds
      !         I: maxmimum infiltration rate from ponds
      !-----------------------------------------

      ! Q: runoff calculation
      zrunoff=MAX(0.0,zrain-rwaterfrac_ia*rwaterfrac_S)**2/(zrain+(1.0-rwaterfrac_ia)*rwaterfrac_S)

      ! K.dt ! when w=0 infinite - minimum for safety.
      zpud1=rwaterfrac_rate*dt !/(MAX(reps,rpuddle(ix,iy)))**rwaterfrac_shapep2

      ! f factor=w/w_max
      zpud2=rpuddle(ix,iy)/rwaterfrac_max

      ! in this solution we incorporate 1/sqrt(w) in the K term
      rpuddle(ix,iy)=(rpuddle(ix,iy)+zpud1*(1.0-zpud2)*zrunoff)/ &
      &  (1.0+zpud1*(rwaterfrac_evap+zpud2*rwaterfrac_infil130))
!      rpuddle(ix,iy)=(rpuddle(ix,iy)+zpud1*zrunoff)/ &
!      &  (1.0+zpud1*(rwaterfrac_evap+zpud2*rwaterfrac_infil130+zrunoff/rwaterfrac_max))

    CASE DEFAULT
      print *,'no default option for puddle - please set ipuddle correctly'
      stop
    END SELECT

    ! convert back to prognostic waterfrac:
    rwaterfrac(ix,iy)=rpuddle(ix,iy)+rwaterperm(ix,iy)
    rwaterfrac(ix,iy)=MIN(MAX(rwaterfrac(ix,iy),0.0),rwaterfrac_max) ! safety

    !-----------------------------------------------
    ! 9. Larvae maturation - limitation of resourses
    !-----------------------------------------------
    ! limitation of resources - negative feedback on numbers
    ! integrate biomass of larvae - bomblies instead slows grow rate 
    ! from v1.3.5 added rwateroccupancy, which gives fractional occupancy by LU type.
    zbiolimit=rbiocapacity*rwaterfrac(ix,iy) !*rwateroccupancy(ix,iy)
    zcapacity=MIN(MAX((zbiolimit-zmasslarv)/zbiolimit,0.01),1.0)

    !-------------------------------------------
    ! 10. Larvae maturation - larvae progression
    !-------------------------------------------
    ! larvae maturation progress rate  
    ! degree day concept of Detinova (1962) again:

    ! egg development and pupae development are all around one day, 
    ! therefore the immature phase length is mostly controlled by the 
    ! immature phase.
    IF (ztempwater>rlarv_tmin .AND. ztempwater <rlarv_tmax) THEN
      zlarvmaturef=rlarvmature(nlarv_scheme,1)*ztempwater+rlarvmature(nlarv_scheme,2)
      zlarvmaturef=zlarvmaturef/(1.0+zlarvmaturef*(rlarv_eggtime+rlarv_pupaetime))
      zlarvmaturef=zlarvmaturef*dt ! timestep 
      zlarvmaturef=MIN(MAX(0.0,zlarvmaturef),1.0)
    ELSE
      zlarvmaturef=0.0
    ENDIF

    ! Bomblies slowed development due to overcrowding but 
    ! this causes unrealistically long development
    ! times, so instead we use the overcrowding factor 
    ! to alter the mortality rate for larvae
    !!!! zlarvmaturef=zlarvmaturef*zcapacity
    IF (zlarvmaturef>reps) CALL advection(1.0, zlarvmaturef,rlarv(:,ix,iy),nlarv)

    IF (ltimer) CALL timer('larv',icheck,time1,time2) ! cpu timer

    !-----------------------------------------
    ! 11. Larvae mortality
    !-----------------------------------------
    ! larvae mortality is due to three factors
    ! 1. canabalism - L4 stage feeds on L1
    ! 2. mortality from preditors - after pool establishment (time delay)
    ! 3. Dessication - if waterfrac reduces over a timestep, 
    !                  the larvae are reduced by the same frac
    !                  this of course assumes that the pool are independent

    zsurvp=MAX(0.0,rlarvsurv-1.0/(MAX(rlarv_tmax-ztempwater,1.0)**2))
    zsurvp=zsurvp*zcapacity ! Bomblies type capacity limitation
!   cannibalism here if required

    !-----------------------------
    ! dessication by rain FLUSHING
    ! ----------------------------
    zflushr=(1.0-rlarv_flushmin)*EXP(-zrain/rlarv_flushtau)+rlarv_flushmin

    DO i=0,nlarv
    ! greatest flushing to L1 larvae so apply as a linear function
      zlimit=REAL(i)/REAL(nlarv)
      zsurvpl=zsurvp*(zlimit*(1.0-zflushr)+zflushr)
      zsurvpl=zsurvpl**dt ! timestep
      zsurvpl=MIN(MAX(0.0,zsurvpl),1.0)
      rlarv(i,ix,iy)=rlarv(i,ix,iy)*zsurvpl
    ENDDO

    !-----------------------------------------
    ! 12. Larvae hatching
    !-----------------------------------------
    rvect(0,0,ix,iy)=rvect(0,0,ix,iy)+rlarv(nlarv,ix,iy)
    rlarv(nlarv,ix,iy)=0.0    
    
    IF (ltimer) CALL timer('end1',icheck,time1,time2) ! cpu timer

    !------------------------------------------------------------------------------------
    ! 13. Migration of population and vectors
    !------------------------------------------------------------------------------------
    !     for the moment - consider a simple migration from "outside" as a trickle source
    !     a second option will be introduced to represent migration in the gridded world
    !     !------------------------------
    ! options:
    ! a. method 1: do not allow uninfected to exceed 99.5 % (remove in postprocessing)
    !
    ! b.  use the gravity model as in the cholera model 
    !     M=Pop(a).Pop(b)/(distance a-b) (set up matrix value before)
    !     but the matrix will limit maximum distance - small distances 
    !     will be set to zero since workers return to sleep in same 
    !     location each night when transmission occurs.
    !
    ! c. Full agent based model (for 2013)
    !
    !------------------------------------------------------------------------------------
    IF (rmigration>reps) THEN
      zdel=MAX(rmigration-1.0+rhost(0,nadult_ni,ix,iy),0.0)
      rhost(ninfh,nadult_ni,ix,iy)=rhost(ninfh,nadult_ni,ix,iy)+zdel
      rhost(0,nadult_ni,ix,iy)    =rhost(0,nadult_ni,ix,iy)    -zdel
    ENDIF

    IF (lascii) THEN
      PRINT *,'ascii output no longer supported from v1.3.3'
      lascii=.false.
    ENDIF
    IF (ltimer) CALL timer('diag',icheck,time1,time2) ! cpu timer
    !--------------------
    ! END OF SPATIAL LOOP
    !--------------------
    ELSE
      rdiag2d(ix,iy,:)=rfillvalue
    ENDIF !non-lake or sea point
    ENDDO !nlon
    ENDDO !nlat

    !------------
    ! 15. NCDF OUTPUT
    !------------
    IF (.NOT.lspinup) THEN
      CALL writedata(iday)
    ENDIF
  ENDDO ! date loop
  CALL setdown
  WRITE(iounit,*) 'integration finished'

!---------------------------------------
CONTAINS
  SUBROUTINE timer(str,icheck,time1,time2)

  IMPLICIT NONE

  REAL :: time1,time2
  INTEGER :: icheck
  CHARACTER*4 :: str

  CALL cpu_time(time2)
  PRINT *,'Check point ',icheck,str,1000*time2-time1
  time1=time2
  icheck=icheck+1

  RETURN
  END SUBROUTINE timer

END PROGRAM VECTRI
